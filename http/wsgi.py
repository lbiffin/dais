import os, sys, logging

from os.path import dirname, realpath

from ConfigParser import ConfigParser


log = logging.getLogger(__name__)


class WSGIServer(object):
    """ Provide a contained WSGI app and utilities.
        Author: Lorren Biffin (lbiffin@furvr.co) """

    application = None
    app_main = None
    config_ini = None
    config = None

    def __init__(self, ini, main):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        self.config_ini = ini
        self.app_main = main

        self._load_config()
        self._create_app()
        self._create_app()

    def _load_config(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        self.config = ConfigParser()
        self.config.read(self.config_ini)

    def _create_app(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        self.application = self.app_main(self.config)

    def get_app(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        return self.application
