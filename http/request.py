import logging, time, os


log = logging.getLogger(__name__)


def get_dbs(DBSession):
    """ Create database session for current session.
        Author: Lorren Biffin (lorrenb@motiga.com) """

    log.debug('Created callback for get_dbs')
    def create_dbs(request):
        log.debug('Created database session for use in current session context')
        return DBSession()
    return create_dbs


def get_setting(request):
    """ Return an accessor method to the settings registry so we can access 
        settings directly by key via `req.settings(..)`
        Author: Lorren Biffin (lbiffin@furvr.co) """
    log.debug('Preparing settings variable request getter.')

    def _get_setting(key):
        setting = request.registry.settings.get(key)

        if (setting is None):
            log.debug('Setting for key `%s` not found.' % (key))
        
        return setting

    return _get_setting
