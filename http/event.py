import logging

log = logging.getLogger(__name__)


def init_acl(DBSession):
    """ Doesn't actually do anything yet.
        Author: Lorren Biffin (lorrenb@motiga.com) """
    pass


def identify_request(_logger):
    def _identify_request(context):
        """ Handle head requests (so we don't use unnecessary requests) """

        req = context.request

        url = req.url
        method = req.method
        client_addr = req.client_addr

        _logger.debug('Request from %s: %s %s', client_addr, method, url)
    
    return _identify_request