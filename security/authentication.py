from pyramid.authentication import AuthTktAuthenticationPolicy as PyramidAuthTktAuthenticationPolicy, AuthTktCookieHelper as PyramidAuthTktCookieHelper
from publicsuffix import PublicSuffixList

class AuthTktAuthenticationPolicy(PyramidAuthTktAuthenticationPolicy):
    """ Adds the sibling_domains options that gets passed to the AuthTktCookieHelper.
        Ref: https://gist.github.com/landreville/885d5b21295bc4fb192d """
    pass

class AuthTktCookieHelper(PyramidAuthTktCookieHelper):
    """ Adds the option to include "sibling" domains in the auth ticket cookie.
        Ref: https://gist.github.com/landreville/885d5b21295bc4fb192d """
    pass