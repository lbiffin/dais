#!/bin/bash

# Before you can use this, cd to /furvr/tools/dais and execute:
# 	$ chmod +x dev.sh

function stat_msg {
	MSG=" * $1"
	RES="${2-0}"

	printf "${MSG}"
	
	if [ -n "$TERM" ] && [ "$TERM" = unknown ] ; then
		echo ""
		if [ "$RES" == "1" ]; then
			echo -e "   ...REKT"
		else
			echo -e "   ...FAIL"
		fi
	fi
	if [ -z $COLUMNS ] && [ -n "$TERM" ] && [ "$TERM" != "unknown" ] && [ -n "`which tput`" ] ; then
		RED=$(tput setaf 1)
		GREEN=$(tput setaf 2)
		NORMAL=$(tput sgr0)
		let COL=$(tput cols)-${#MSG}+10

		if [ "$RES" == "1" ]; then
			printf "%${COL}s\n" "$GREEN[REKT]$NORMAL"
		else
			printf "%${COL}s\n" "$RED[FAIL]$NORMAL"
		fi
	fi
}

function restart_service {
	# Default response is cynical
	result=0

	# What it do?
	echo -e "Gettin' at \`$1\`:"

	# Attempt to find a case where we can restart
	# Define special cases using explicit case blocks .. default to service
	# calls for standard restarts
	case "$1" in
		uwsgi)
			if killall -9 uwsgi; then
				let result=1
			fi ;;
		*)
			if service $1 restart; then
				let result=1
			fi ;;
	esac

	# Response still bad?
	if [ "$result" -eq 0 ]; then # Yep ..
		stat_msg "Service \`$1\` is 2 legit to quit (stop|start)" $result
	else # Nope ..
		stat_msg "Service \`$1\` aint shit." $result
	fi

	return $result
}

function quit {
	echo "** mic drop **"
	exit
}

# Restart Services / Processes
echo "Restarting services:"
restart_service uwsgi
restart_service nginx
restart_service varnish

quit