import urllib, logging
from markupsafe import Markup

log = logging.getLogger(__name__)


def urlencode_filter(s):
    if type(s) == 'Markup':
        s = s.unescape()
    s = s.encode('utf8')
    s = urllib.quote_plus(s)
    return Markup(s)
