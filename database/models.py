
import json, logging

from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.dialects import postgresql as psql


@declarative_base
class Base(object):
    """ Setup base model inheritence.
        Author: Lorren Biffin (lbiffin@furvr.co) """


class Jsonable(object):
    """ Define the method with which a model is converted to JSON output.
        Author: Lorren Biffin (lbiffin@furvr.co) """

    __jsonable__ = ['id']
    
    @property
    def columns(self):
        """ Return a list of available columns.
            Author: Lorren Biffin (lbiffin@furvr.co) """
        return [ c for c in self.__jsonable__ ]

    @property
    def columnitems(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        return dict([ (c, getattr(self, c)) for c in self.columns ])

    def __repr__(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        return '{}({})'.format(self.__class__.__name__, self.columnitems)

    def json(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        return self
