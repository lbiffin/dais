import unittest

from dais.tests.http.wsgi.server import TestWSGIServer


if __name__ == '__main__':
    """ Run all tests in the context of the Dais application ..
        https://docs.python.org/2/library/unittest.html#unittest.TestSuite
        TODO: Make these run with a TestSuite
        Author: Lorren Biffin (lbiffin@furvr.co) """
    unittest.main()