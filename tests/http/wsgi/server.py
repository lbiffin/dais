import unittest, os

from ConfigParser import ConfigParser

from dais.http.wsgi import WSGIServer


def main_dummy(config):
    """ Author: Lorren Biffin (lbiffin@furvr.co) """
    def wsgi_app_dummy():
        pass
    return wsgi_app_dummy


class TestWSGIServer(unittest.TestCase):
    """ Test cases for Dais WSGIServer class. To test individual WSGI
        applicaitons, simply import this test into that test and overwrite
        the `_get_ini_path` and `_get_app_main` methods.
            -   TODO: Seperate the test base from the actual test thing.
            -   TODO: Create a test case for logging configuration.
        Author: Lorren Biffin (lbiffin@furvr.co) """

    _server = None

    def _get_ini_path(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        here = os.path.realpath(os.path.dirname(__file__))
        return '%s/files/config.ini' % (here)

    def _get_app_main(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        return main_dummy

    def setUp(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        _ini = self._get_ini_path()
        _main = self._get_app_main()

        self._server = WSGIServer(ini=_ini, main=_main)
    
    def test_HasConfig(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        
        self.assertTrue(self._server.config is not None, \
            "Application's config does not exist.")
        
        self.assertTrue(isinstance(self._server.config, ConfigParser), \
            "Application's config is not a ConfigParser object.")
    
    def test_HasAppMain(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        
        self.assertTrue(self._server.app_main is not None, \
            "Application's `main` function does not exist.")
        
        self.assertTrue(callable(self._server.app_main), \
            "Application's `main` function is not callable.")
    
    def test_IsApplication(self):
        """ Check whether the thing is awesome.
            Author: Lorren Biffin (lbiffin@furvr.co) """
        
        self.assertTrue(self._server.application is not None, \
            "Application does not exist.")

    def tearDown(self):
        """ Author: Lorren Biffin (lbiffin@furvr.co) """
        self._server = None


if __name__ == '__main__':
    unittest.main()